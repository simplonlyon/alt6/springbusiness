package co.simplon.alt6.springbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.springbusiness.entity.Animal;

@Repository
public interface AnimalRepository extends JpaRepository<Animal,Integer> {
    
}
