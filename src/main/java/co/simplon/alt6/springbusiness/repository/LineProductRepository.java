package co.simplon.alt6.springbusiness.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.springbusiness.entity.LineProduct;
import co.simplon.alt6.springbusiness.entity.ProductItem;
import co.simplon.alt6.springbusiness.entity.User;

@Repository
public interface LineProductRepository extends JpaRepository<LineProduct, Integer>{
    
    Optional<LineProduct> findByProductItemAndUser(ProductItem productItem, User user);
}
