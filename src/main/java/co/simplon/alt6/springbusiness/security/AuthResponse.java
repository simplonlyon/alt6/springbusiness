package co.simplon.alt6.springbusiness.security;

import org.springframework.security.core.userdetails.UserDetails;

public class AuthResponse {
    public AuthResponse(UserDetails user, String token) {
        this.user = user;
        this.token = token;
    }
    private UserDetails user;
    private String token;
    public UserDetails getUser() {
        return user;
    }
    public void setUser(UserDetails user) {
        this.user = user;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
}
