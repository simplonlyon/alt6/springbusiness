package co.simplon.alt6.springbusiness.security;


import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Ce Hasher n'est pas à utiliser dans la vraie vie (préférer BCrypt ou Argon2I ou autre selon les recommandations de sécurité du moment)
 * il est juste là pour montrer le processus de Hash, génération du sel, utilisation du poivre et comparaison
 * entre un mdp hashé et un mdp en clair
 */
@Component
public class Hasher implements PasswordEncoder {

    @Value("${security.pepper}")
    private String pepper;

    /**
     * Cette méthode prend un mot de passe en clair, utilise une library pour générer une chaîne
     * de caractère random qui sera notre sel, puis utilise l'algorithme SHA256 pour hasher le
     * mot de passe concaténé avec le sel et le poivre (qui est défini dans le application.propertie, qui pourrait
     * être défini en variable d'environnemnet ou autre)
     */
    @Override
    public String encode(CharSequence rawPassword) {
            String salt = RandomStringUtils.randomAlphanumeric(10);            
            //On fait en sorte de renvoyé le hash avec le sel concaténé avant
            return salt+DigestUtils.sha256Hex(salt+rawPassword+pepper);

    }

    /**
     * Cette méthode prend un mot de passe en clair et un mot de passe hashé et va récupérer le sel
     * dans le mot de passe hashé (en faisant un substring, vu qu'on sait quel taille fait notre sel)
     * On concatène ce sel au mot de passe en clair, on lui ajoute le poivre et on réutilise le même algorithme
     * (SHA256) pour générer un hash qu'on compare avec le hash qui aura été précédemment stocké
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        
        String salt = encodedPassword.substring(0, 10);
        String hash = encodedPassword.substring(10);
        String rehashed = DigestUtils.sha256Hex(salt+rawPassword+pepper);

        return rehashed.equals(hash);
    }
    
}
