package co.simplon.alt6.springbusiness.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

@Service
public class JwtUtils {
    @Value("${jwt.secret}")
    private String secret;

    public String generateJwt(UserDetails user) {
        return JWT.create()
        .withClaim("email",user.getUsername())
        .withClaim("role", user.getAuthorities().toString())
        .withIssuedAt(new Date())
        .withExpiresAt(new Date(System.currentTimeMillis()+60*60*1000))
        .sign(Algorithm.HMAC256(secret));
    }

    public String validateAndGetUsername(String token) {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret)).build();
        DecodedJWT decoded = verifier.verify(token);

        return decoded.getClaim("email").asString();
    }
}
