package co.simplon.alt6.springbusiness.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
public class SecurityConfig {
	@Autowired
	private JwtFilter jwtFilter;
    
    @Bean
    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
        //Temporairement on configure Spring Security pour autoriser l'accès à toutes les routes
        return http.authorizeHttpRequests((authorize) -> 
			authorize
			.requestMatchers("/api/account").authenticated()
			.anyRequest().permitAll())
		.cors((cors) -> cors.disable()) //On désactive les CORS ce qui signifie que n'importe quel site pourra faire des requêtes vers l'api depuis JS
		.csrf((csrf) -> csrf.disable()) //On désactive la protection CSRF qui est de toute façon une faille qui ne pourra pas nous atteindre du fait qu'on utilisera pas un cookie de session mais un JWT et une serveur stateless
		.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
        .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
		.build();
    }

	//Si on voulait utiliser un vrai password encoder, ce qui est recommandé dans la vraie vie
	// @Bean
	// public PasswordEncoder passwordEncoder() {
	// 	return new BCryptPasswordEncoder(12);
	// }
}
 
