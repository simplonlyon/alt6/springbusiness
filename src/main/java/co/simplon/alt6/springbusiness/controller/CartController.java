package co.simplon.alt6.springbusiness.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt6.springbusiness.business.CartBusiness;

@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private CartBusiness cart;

    @PostMapping("/{idItem}") 
    public void add(@PathVariable int idItem, @RequestParam(required = false, defaultValue = "1") int quantity) {
        try {
            cart.addToCart(1, idItem, quantity);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping
    public void validate() {
        try {
            cart.validateCart(1);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
