package co.simplon.alt6.springbusiness.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt6.springbusiness.business.AuthBusiness;
import co.simplon.alt6.springbusiness.entity.User;
import co.simplon.alt6.springbusiness.security.AuthResponse;
import co.simplon.alt6.springbusiness.security.Credentials;
import co.simplon.alt6.springbusiness.security.JwtUtils;

@RestController
public class AuthController {
    @Autowired
    private AuthBusiness authBusiness;
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/api/register")
    public User postUser(@RequestBody User user){
        try {
            authBusiness.register(user);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return user;
    }

    @PostMapping("/api/login")
    public AuthResponse postLogin(@RequestBody Credentials credentials) {
        
        UserDetails user = authBusiness.loadUserByUsername(credentials.email);
        if(!passwordEncoder.matches(credentials.password, user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login/Password doesn't match or exist");
        }
        return new AuthResponse(user, jwtUtils.generateJwt(user));
    }

    @GetMapping("/api/account")
    public UserDetails getAccount(@AuthenticationPrincipal UserDetails user) {
        return user;
    }
}
