package co.simplon.alt6.springbusiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbusinessApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(SpringbusinessApplication.class, args);
	}

}
