package co.simplon.alt6.springbusiness;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import co.simplon.alt6.springbusiness.entity.Animal;
import co.simplon.alt6.springbusiness.entity.Color;
import co.simplon.alt6.springbusiness.entity.Product;
import co.simplon.alt6.springbusiness.entity.ProductItem;
import co.simplon.alt6.springbusiness.entity.User;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@Component
@Transactional
public class DataLoader implements ApplicationRunner {
    @Autowired
    private EntityManager em;
    @Autowired
    private PasswordEncoder hasher;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        User user = new User();
        user.setEmail("test@test.com");
        user.setPassword(hasher.encode("1234"));
        //Dans le cas où on aurait beaucoup de user à créer et donc beaucoup de mot de passe à hahser, soit on le hash une fois avec l'encoder et on le réutilise, soit on récupère directement un mot de passe hashé et on l'assigne au User, sachant que si on change d'algo de hash, il faudra le modifier
        // user.setPassword("Sg828h1x813a826d9938781b374e6cf9b59c6394ced0af593d66640b1ad891b3649bc937e7");
        user.setRole("ROLE_USER");
        em.persist(user);

        List<Product> products = new ArrayList<>();
        List<Color> colors = new ArrayList<>();
        List<Animal> animals = new ArrayList<>();

        for (int i = 1; i <= 5; i++) {
            Product prod = new Product();
            prod.setLabel("Product"+i);
            prod.setPrice(i*10d);
            prod.setDescription("Description");
            em.persist(prod);
            products.add(prod);

            Color color = new Color();
            color.setLabel("Color"+i);
            em.persist(color);
            colors.add(color);

            Animal animal = new Animal();
            animal.setBreed("Breed "+i);
            em.persist(animal);
            animals.add(animal);

        }
        
        for (Product product : products) {
            for (int i = 0; i < 5; i++) {
                ProductItem stock = new ProductItem();
                stock.setAnimal(animals.get(i));
                stock.setColor(colors.get(i));
                stock.setProduct(product);
                stock.setSize("small");
                stock.setStock(3);
                em.persist(stock);
            }
        }

    }
    
}
