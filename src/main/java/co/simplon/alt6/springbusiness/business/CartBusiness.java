package co.simplon.alt6.springbusiness.business;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplon.alt6.springbusiness.entity.LineProduct;
import co.simplon.alt6.springbusiness.entity.Order;
import co.simplon.alt6.springbusiness.entity.ProductItem;
import co.simplon.alt6.springbusiness.entity.User;
import co.simplon.alt6.springbusiness.repository.LineProductRepository;
import co.simplon.alt6.springbusiness.repository.OrderRepository;
import co.simplon.alt6.springbusiness.repository.ProductItemRepository;
import co.simplon.alt6.springbusiness.repository.UserRepository;
import jakarta.transaction.Transactional;

@Service
public class CartBusiness {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductItemRepository stockRepository;
    @Autowired
    private LineProductRepository lineProductRepository;
    @Autowired
    private OrderRepository orderRepository;

    public void addToCart(int idUser, int idItem, int quantity) throws Exception {
        User user = userRepository.findById(idUser)
                .orElseThrow(() -> new Exception("User doesn't exist"));
        ProductItem item = stockRepository.findById(idItem)
                .orElseThrow(() -> new Exception("Product doesn't exist"));
        Optional<LineProduct> existing = lineProductRepository.findByProductItemAndUser(item, user);

        if (existing.isPresent()) {
            LineProduct lineProduct = existing.get();
            lineProduct.setQuantity(lineProduct.getQuantity()+quantity);
            lineProductRepository.save(lineProduct);
        } else {

            LineProduct lineProduct = new LineProduct();
            lineProduct.setUser(user);
            lineProduct.setQuantity(quantity);
            lineProduct.setPrice(item.getProduct().getPrice());
            lineProduct.setProductItem(item);
            lineProductRepository.save(lineProduct);

        }
    }

    //La transaction permet de faire que si une Exception est déclenchée dans cette méthode, toutes les opérations base de données effectuées sont annulées
    @Transactional(rollbackOn = Exception.class)
    public void validateCart(int idUser) throws Exception {
        User user = userRepository.findById(idUser)
                .orElseThrow(() -> new Exception("User doesn't exist"));

        if(user.getCart().isEmpty()) {
            throw new Exception("Cart is empty");
        }

        //On créer une commande qu'on assigne au User    
        Order order = new Order();
        order.setUser(user);
        order.setOrderDate(LocalDate.now());
        order.setStatus("pending");
        orderRepository.save(order);
        //On parcours le panier du user et pour chaque élément du panier on vérifie si le stock disponible
        //est satisfaisant par rapport à la quantité qui avait été mise au panier
        for(LineProduct line : user.getCart()) {
            if(line.getQuantity() > line.getProductItem().getStock()) {
                throw new Exception("Not enough stock");
            }
            //On retire la ligne du panier du User, on décrémente le stock et on assigne la ligne à la commande
            line.setUser(null);
            line.getProductItem().setStock(line.getProductItem().getStock()-line.getQuantity());
            line.setOrder(order);
            lineProductRepository.save(line);
        }




    }
}
