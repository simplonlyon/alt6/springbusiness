package co.simplon.alt6.springbusiness.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import co.simplon.alt6.springbusiness.entity.User;
import co.simplon.alt6.springbusiness.repository.UserRepository;

@Service
public class AuthBusiness implements UserDetailsService{
    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserRepository userRepo;

    public void register(User user) throws Exception {
        //A la place de ce bout de code ci dessous, on pourrait utiliser notre loadUserByUsername
        Optional<User> existingUser = userRepo.findByEmail(user.getEmail());
        if(existingUser.isPresent()) {
            throw new Exception("User already exist");
        }

        String hashedPassword = hasher.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setRole("ROLE_USER");
        userRepo.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        return userRepo.findByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException("User doesn't exist"));
    }
}
