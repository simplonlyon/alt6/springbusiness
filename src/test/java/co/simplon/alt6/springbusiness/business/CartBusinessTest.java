package co.simplon.alt6.springbusiness.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import co.simplon.alt6.springbusiness.entity.LineProduct;
import co.simplon.alt6.springbusiness.entity.Product;
import co.simplon.alt6.springbusiness.entity.ProductItem;
import co.simplon.alt6.springbusiness.entity.User;
import co.simplon.alt6.springbusiness.repository.LineProductRepository;
import co.simplon.alt6.springbusiness.repository.OrderRepository;
import co.simplon.alt6.springbusiness.repository.ProductItemRepository;
import co.simplon.alt6.springbusiness.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
public class CartBusinessTest {
    @Mock
    OrderRepository orderRepo;
    @Mock
    UserRepository userRepo;
    @Mock
    ProductItemRepository productItemRepo;
    @Mock
    LineProductRepository lineRepo;

    @InjectMocks
    CartBusiness cartBusiness;

    ProductItem item;
    User user;
    @BeforeEach
    void setUp() {
        user = new User();
        item = new ProductItem();
        Product product = new Product();
        product.setPrice(10d);
        item.setProduct(product);
        Mockito.when(userRepo.findById(1)).thenReturn(Optional.of(user));
        Mockito.when(productItemRepo.findById(1)).thenReturn(Optional.of(item));
    }

    @Test
    void testAddToCartNotInCart() throws Exception {
        cartBusiness.addToCart(1, 1, 1);

        ArgumentCaptor<LineProduct> captor = ArgumentCaptor.forClass(LineProduct.class);
        Mockito.verify(lineRepo).save(captor.capture());

        assertEquals(1, captor.getValue().getQuantity());
        assertEquals(user, captor.getValue().getUser());
        assertEquals(item, captor.getValue().getProductItem());
        assertEquals(10d, captor.getValue().getPrice());

    }

    @Test
    void testAddToCartAlreadyInCart() throws Exception {
        LineProduct line = new LineProduct();
        line.setQuantity(3);
        line.setProductItem(item);
        line.setUser(user);
        line.setPrice(10d);
        
        Mockito.when(lineRepo.findByProductItemAndUser(item, user)).thenReturn(Optional.of(line));
        
        cartBusiness.addToCart(1, 1, 1);
        ArgumentCaptor<LineProduct> captor = ArgumentCaptor.forClass(LineProduct.class);
        Mockito.verify(lineRepo).save(captor.capture());

        assertEquals(line, captor.getValue());
        assertEquals(4, captor.getValue().getQuantity());
        assertEquals(user, captor.getValue().getUser());
        assertEquals(item, captor.getValue().getProductItem());
        assertEquals(10d, captor.getValue().getPrice());

    }
    @Test
    void testValidateCart() {

    }
}
